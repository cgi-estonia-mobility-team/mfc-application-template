#include "FlowControllerFactory.h"

#include <stdexcept>

std::map<std::string, FlowControllerFactory::CreateFlowControllerDelegate> FlowControllerFactory::s_flowControllerCreatorDelegates;

std::auto_ptr<FlowController> FlowControllerFactory::create(const std::string& flowName,
															CWnd& rootView,
															FlowController* parent)
{
	FlowControllerCreatorMap::const_iterator it = s_flowControllerCreatorDelegates.find(flowName);
    if (it == s_flowControllerCreatorDelegates.end())
		throw std::invalid_argument(flowName + ": unregistered flow controller");

    return (it->second)(rootView, parent);
}

void FlowControllerFactory::registerFlowControllerCreator(const std::string& flowName,
            CreateFlowControllerDelegate delegateFunction)
{
	s_flowControllerCreatorDelegates[flowName] = delegateFunction;
}