#pragma once

#include <utilcpp/declarations.h>

class FlowController
{
	UTILCPP_DECLARE_INTERFACE(FlowController)

public:
	virtual void start() = 0;
	virtual void end() = 0;
	virtual void goForward() = 0;
	virtual void goBack() = 0;
};
