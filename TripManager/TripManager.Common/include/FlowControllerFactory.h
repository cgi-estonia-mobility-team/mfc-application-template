#pragma once

#include "FlowController.h"

#include <memory>
#include <string>
#include <map>

class CWnd;

class FlowControllerFactory
{
public:
	typedef std::auto_ptr<FlowController> (*CreateFlowControllerDelegate)(CWnd& rootView, FlowController* parentFlow);
	typedef std::map<std::string, CreateFlowControllerDelegate> FlowControllerCreatorMap;

	static void registerFlowControllerCreator(const std::string& flowName,
            CreateFlowControllerDelegate delegateFunction);

	static std::auto_ptr<FlowController> create(const std::string& flowName, CWnd& rootView,
		FlowController* parentFlow = NULL);

private:
	FlowControllerFactory();

	static FlowControllerCreatorMap s_flowControllerCreatorDelegates;
};
