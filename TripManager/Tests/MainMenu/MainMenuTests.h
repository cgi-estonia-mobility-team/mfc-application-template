#pragma once

#include "TripManager.MainMenu/src/Model/MenuItem.h"
#include "TripManager.MainMenu/src/Model/MenuItemRepository.h"

#include "TripManager.Utils/include/utils/DbHelper.h"

#include <testcpp/testcpp.h>
#include <utilcpp/disable_copy.h>

class Menu
{
public:
	Menu() :
		m_items(MenuItemRepository::GetAll())
	{ }

	const MenuItem::list& items()
	{ return m_items; }

private:
	MenuItem::list m_items;
};

class MainMenuTests : public Test::Suite
{
    UTILCPP_DISABLE_COPY(MainMenuTests)

public:

	MainMenuTests()
	{
		// constructor is for setup, destructor is for teardown
		DbHelper::initializeDatabase(DB_FILE, fillMenu);
	}

	void test()
	{
		// test() is the "main" of the test suite - call all tests from here
		testMenuContainsExpectedItems();
	}

	void testMenuContainsExpectedItems()
	{
		// follow the Arrange-Act-Assert structure in each test
		// http://c2.com/cgi/wiki?ArrangeActAssert

		// arrange
		Menu menu;

		// no act - menu constructor takes care of filling the menu

		// assert
		Test::assertEqual("Menu contains expected items",
                getDefaultMenuItems(), menu.items());
	}


private:


	static MenuItem::list getDefaultMenuItems()
	{
		MenuItem::list items;
		items.push_back(MenuItem("Show trips", "ShowTrips"));
		return items;
	}

	static void fillMenu()
	{
		MenuItemRepository::CreateTable();
		MenuItem::list menuitems = getDefaultMenuItems();
		MenuItemRepository::Save(menuitems);
	}

	static const char* const DB_FILE;
};

const char* const MainMenuTests::DB_FILE = "TripManager.sqlite";
