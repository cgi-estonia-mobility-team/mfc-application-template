// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <windows.h>
#include <tchar.h>

// #include <boost/lambda/lambda.hpp>

#include <algorithm>
#include <vector>
#include <string>
#include <fstream>
#include <iostream>

#include <cstdio>
