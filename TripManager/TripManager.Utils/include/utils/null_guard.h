#pragma once

#include <utilcpp/release_assert.h>

template<class T>
class null_guard
{

private:

    typedef null_guard<T> this_type;

public:

    explicit null_guard(T* p = 0) :
        _ptr(p)
    { }

	this_type(const this_type& other)
    {
        UTILCPP_RELEASE_ASSERT(other != 0,
                "Null pointer assignment in null_guard");
		_ptr = other._ptr;
	}

    this_type& operator=(this_type other)
    {
        UTILCPP_RELEASE_ASSERT(other != 0,
                "Null pointer assignment in null_guard");
		_ptr = other._ptr;
        return *this;
    }

    T& operator*() const
    {
        UTILCPP_RELEASE_ASSERT(_ptr != 0,
                "Dereferencing null pointer in null_guard");
        return *_ptr;
    }

    T* operator->() const
    {
        UTILCPP_RELEASE_ASSERT(_ptr != 0,
                "Dereferencing null pointer in null_guard");
        return _ptr;
    }

    T* get() const
    {
        return _ptr;
    }

    operator bool() const
    {
        return _ptr != 0;
    }

private:

    T* _ptr;

    void operator==(const null_guard&) const;
    void operator!=(const null_guard&) const;
};
