#pragma once

#define CATCH_GENERIC_ERRORS \
	catch (const std::exception& e) \
	{ AfxMessageBox(CString(_T("Error occurred: ")) + CString(e.what())); } \
	catch (...) \
	{ AfxMessageBox(_T("Unknown error occurred")); }