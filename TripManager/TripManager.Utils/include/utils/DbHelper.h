#pragma once

#include <cstdio>

namespace DbHelper
{

inline bool doesFileExist(const char* const filename)
{
	FILE* f;
	errno_t ret = fopen_s(&f, filename, "r");
	if (f)
		fclose(f);
	return ret == 0;
}

typedef void (*DbInitializationCallback)(void);

inline void initializeDatabase(const char* dbFile, DbInitializationCallback initFunction)
{
	bool dbExists = doesFileExist(dbFile);

	dm::sql::ConnectDatabase(dbFile);

	if (!dbExists)
		initFunction();
}

}
