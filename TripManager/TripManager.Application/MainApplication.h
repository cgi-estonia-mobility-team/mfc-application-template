
// TripManagerUI.h : main header file for the TripManagerUI application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// MainApplication:
// See TripManagerUI.cpp for the implementation of this class
//

class MainApplication : public CWinApp
{
public:
	MainApplication();


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

public:
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern MainApplication theApp;
