#pragma once

#include <datamappercpp/sql/Repository.h>
#include <datamappercpp/sql/db.h>

#include "MenuItem.h"
#include "MenuRepository.h"

class MenuItemMapping
{
public:
	static std::string getLabel()
	{ return "menu_item"; }

	template <class Visitor>
	static void accept(Visitor& v, MenuItem& item)
	{
		v.visitField(dm::Field<std::string>("label", "UNIQUE NOT NULL"), item.label);
		v.visitField(dm::Field<std::string>("workflow",  "NOT NULL"),    item.workflow);
	}

	static std::string customCreateStatements()
	{ return ""; }

private:
	MenuItemMapping();
};

class MenuItemRepository :
	public dm::sql::Repository<MenuItem, MenuItemMapping>
{ };