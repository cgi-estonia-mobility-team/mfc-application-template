#pragma once

#include "MenuRepository.h"

class MenuRepositoryImpl : public MenuRepository
{
public:
	virtual ~MenuRepositoryImpl() {}

	virtual MenuItem::list allMenuItems() const;
};