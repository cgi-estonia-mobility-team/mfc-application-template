#pragma once

#include <string>
#include <vector>

struct MenuItem
{
	typedef std::vector<MenuItem> list;

	int id;
	std::string label;
	std::string workflow;

	MenuItem() :
		id(-1), label(), workflow()
	{ }

	MenuItem(const std::string& l, const std::string& wf) :
		id(-1), label(l), workflow(wf)
	{ }

	bool operator== (const MenuItem& other) const
	{
		return label == other.label && workflow == other.workflow; // not comparing ids
	}
};
