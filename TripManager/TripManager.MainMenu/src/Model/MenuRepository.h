#pragma once

#include "MenuItem.h"

#include <utilcpp/declarations.h>

class MenuRepository
{
	UTILCPP_DECLARE_INTERFACE(MenuRepository)

public:
	virtual MenuItem::list allMenuItems() const = 0;
};
