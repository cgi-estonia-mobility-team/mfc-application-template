#include "StdAfx.h"
#include "MenuItemRepository.h"
#include "MenuRepositoryImpl.h"

#include <utils/DbHelper.h>

// a task bean: the functions in this anonymous namespace violate DRY and
// don't belong here - how should this be fixed?
namespace
{

const char* const DB_FILE = "TripManager.sqlite";

void fillMenu()
{
	MenuItemRepository::CreateTable();
	MenuItem::list items;
	items.push_back(MenuItem("Show trips", "ShowTrips"));
	MenuItemRepository::Save(items);
}

bool initializeDatabase()
{
	DbHelper::initializeDatabase(DB_FILE, fillMenu);
	return true;
}

}

MenuItem::list MenuRepositoryImpl::allMenuItems() const
{
	static bool triggerDbInitializationOnlyOnce = initializeDatabase();
	(void *)triggerDbInitializationOnlyOnce;
	return MenuItemRepository::GetAll();
}