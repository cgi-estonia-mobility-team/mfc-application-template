#pragma once

class MenuView;
class MenuRepository;

class MenuPresenter
{
public:
	MenuPresenter(MenuView& view, const MenuRepository& repository) :
	  m_view(view), m_repository(repository) { }
	~MenuPresenter() { }

	void launchNewFlowController(const int selectedItem);
	void initializeView();

private:
	MenuView& m_view;
	const MenuRepository& m_repository;
};
