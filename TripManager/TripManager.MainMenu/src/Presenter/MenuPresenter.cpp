#include "StdAfx.h"
#include "MenuPresenter.h"
#include "../View/MenuView.h"
#include "../Model/MenuRepository.h"

#include <utils/foreach.h>

#include <vector>

void MenuPresenter::initializeView()
{
	m_view.setPresenter(null_guard<MenuPresenter>(this));
	m_view.create();

	std::vector<CString> labels;
	foreach (const MenuItem& item, m_repository.allMenuItems()) {
		labels.push_back(CString(item.label.c_str()));
	}

	m_view.setMenuItems(labels);
}

void MenuPresenter::launchNewFlowController(const int )
{
	AfxMessageBox(_T("This is where you start!"));
}
