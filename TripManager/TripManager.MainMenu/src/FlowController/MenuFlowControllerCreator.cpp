#include "stdafx.h"

#include "MenuFlowControllerCreator.h"
#include "MenuFlowController.h"

std::auto_ptr<FlowController> CreateMainMenuFlowController(CWnd& rootView, FlowController* /* parent flow */)
{
	return std::auto_ptr<FlowController>(new MenuFlowController(rootView));
}