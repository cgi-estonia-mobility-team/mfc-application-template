#pragma once

#include <FlowController.h>
#include "../View/MenuDialog.h"
#include "../Model/MenuRepositoryImpl.h"
#include "../Presenter/MenuPresenter.h"

#include <utilcpp/scoped_ptr.h>

class CWnd;

class MenuFlowController : public FlowController
{
public:
	MenuFlowController(CWnd& rootView);
	virtual ~MenuFlowController();

	virtual void start();

	virtual void end() {}
	virtual void goForward() {}
	virtual void goBack() {}

private:
	CWnd& m_rootView;
	MenuDialog m_view;
	MenuRepositoryImpl m_repository;
	MenuPresenter m_presenter;
};
