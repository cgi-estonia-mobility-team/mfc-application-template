#pragma once

#include <FlowController.h>

#include <memory>

class CWnd;

std::auto_ptr<FlowController> CreateMainMenuFlowController(CWnd& rootView, FlowController* parent);