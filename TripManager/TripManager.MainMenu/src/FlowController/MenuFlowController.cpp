#include "StdAfx.h"
#include "MenuFlowController.h"

MenuFlowController::MenuFlowController(CWnd& rootView) :
	m_rootView(rootView),
	m_view(&rootView),
	m_repository(),
	m_presenter(m_view, m_repository)
{
}

void MenuFlowController::start()
{
	m_presenter.initializeView();
}

MenuFlowController::~MenuFlowController()
{
}
