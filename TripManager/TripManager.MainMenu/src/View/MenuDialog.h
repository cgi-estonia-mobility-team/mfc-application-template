#pragma once
#include "afxwin.h"

#include <vector>
#include <utils/null_guard.h>

#include "MenuView.h"
#include "MenuResource.h"

class MenuPresenter;

// MenuDialog dialog

class MenuDialog : public CDialog, public MenuView
{
	DECLARE_DYNAMIC(MenuDialog)

public:
	MenuDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~MenuDialog();

// Dialog Data
	enum { IDD = IDD_MAIN_MENU };

public:
	afx_msg void OnLbnSelchangeMenuitemList();

public:
	virtual void setPresenter(null_guard<MenuPresenter> const& presenter);

	virtual void setMenuItems(std::vector<CString> const& items);

	virtual void create();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

private:
	null_guard<MenuPresenter> m_presenter;

	CListBox m_menuitemList;
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
};
