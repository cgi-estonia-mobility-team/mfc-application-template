// MenuDialog.cpp : implementation file
//

#include "stdafx.h"
#include "MenuDialog.h"

#include "../Presenter/MenuPresenter.h"

#include <utils/CatchGenericErrors.h>
#include <utils/foreach.h>

// MenuDialog dialog

IMPLEMENT_DYNAMIC(MenuDialog, CDialog)

MenuDialog::MenuDialog(CWnd* pParent /*=NULL*/)
	: CDialog(MenuDialog::IDD, pParent)
{ }

MenuDialog::~MenuDialog()
{ }

void MenuDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MENUITEM_LIST, m_menuitemList);
}


BEGIN_MESSAGE_MAP(MenuDialog, CDialog)
	ON_LBN_SELCHANGE(IDC_MENUITEM_LIST, &MenuDialog::OnLbnSelchangeMenuitemList)
	ON_WM_CREATE()
END_MESSAGE_MAP()


// MenuDialog message handlers

void MenuDialog::OnLbnSelchangeMenuitemList()
{
	// event handlers should use try/catch to avoid leaking exceptions into the MFC message loop
	try {
		m_presenter->launchNewFlowController(m_menuitemList.GetCurSel());
	} CATCH_GENERIC_ERRORS;
}

void MenuDialog::setPresenter(null_guard<MenuPresenter> const& presenter)
{
	m_presenter = presenter;
}

void MenuDialog::setMenuItems(std::vector<CString> const& items)
{
	foreach (CString const& item, items)
	{
		m_menuitemList.AddString(item);
	}
}

int MenuDialog::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (__super::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here

	return 0;
}

void MenuDialog::create()
{
	if (!CDialog::Create((UINT)IDD, m_pParentWnd))
		return;
	ShowWindow(SW_SHOW);
	UpdateWindow();
}
