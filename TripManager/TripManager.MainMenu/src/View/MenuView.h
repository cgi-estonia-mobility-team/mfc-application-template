#pragma once

#include <utilcpp/declarations.h>

#include <vector>
#include <utils/null_guard.h>

class MenuPresenter;

class MenuView
{
	UTILCPP_DECLARE_INTERFACE(MenuView)

public:
	virtual void setPresenter(null_guard<MenuPresenter> const& presenter) = 0;
	virtual void create() = 0;
	virtual void setMenuItems(const std::vector<CString>& items) = 0;
};
